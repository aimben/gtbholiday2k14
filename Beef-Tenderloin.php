<!doctype html>
<?php 
  $pageClass = "beef-tenderloin recipe_pg";
  $pageType = "beef-tenderloin";
  $page2ndType = "beef-tenderloin";
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/config.php"); 
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/head.php"); 
?>  
  </head>


  <body>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">
        <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/nav.php'; ?>
        <a class="exit-off-canvas" href="#"></a>

        <section role="main" id="main">
          <div class="mn_img"></div>
          
          <div class="row" style="">    
             <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/top_and_nav.php'; ?>
            
            <div class="small-12 medium-7 medium-centered text-center main_content columns">
              <h3>The Most Tender Tradition of All</h3>
              <p>
                As the most tender and lean of all roasts, a beef tenderloin roast is a holiday favorite that&prime;s sure to please. Choose from one of our delicious beef tenderloin recipes and don&prime;t forget to utilize our easy cooking tips. The result is going to be tender, juicy and delicious.
              </p>
            </div>
          </div>

          <div class="recipes">
            <div class="row">
              <div class="small-12 columns">
                <dl class="tabs vertical" data-tab data-options="deep_linking:true">
                  <dd class="active"><a href="#Peppercorn-Garlic-Beef-Tenderloin">Peppercorn Garlic Beef Tenderloin</a></dd>
                  <dd><a href="#Chimichurri-Beef-Tenderloin">Chimichurri<br />Beef Tenderloin</a></dd>
                </dl>
                <div class="tabs-content" style="">
                  <div class="content active" id="Peppercorn-Garlic-Beef-Tenderloin" style="">
                    <h3>Perfectly complement your juicy beef tenderloin with the kick of peppercorn and garlic.</h3>
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>Beef Tenderloin</b><br />
                        3-4 lbs Genuine Texas Beef, Beef Tenderloin
                        <br /><br />
                        <b>Marinade</b><br />
                        1 pkg (6 oz) Italian dressing mix<br />
                        1 tsp fresh chopped garlic<br />
                        1 tsp coarsely ground black pepper<br />
                        1/4 cup soy sauce<br />
                        1/4 cup red wine vinegar<br />
                        1/4 cup water<br />
                        1/4 cup oil<br />

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Method</b><br />
Mix marinade ingredients in a medium-size bowl. Place tenderloin in a shallow dish or large resealable storage bag. Pour mixed marinade over beef tenderloin and coat thoroughly. Refrigerate for 2 hours to overnight. Remove beef tenderloin from marinade. Cook on a hot grill on all sides or in a 425&deg; F oven until desired temperature is reached, using a meat thermometer to check (125&deg; F for medium-rare).               
                        <br /><br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Beef-Tenderloin#Peppercorn-Garlic-Beef-Tenderloin" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Beef-Tenderloin#Peppercorn-Garlic-Beef-Tenderloin" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Beef-Tenderloin#Peppercorn-Garlic-Beef-Tenderloin" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>
                  </div>

                  <div class="content" id="Chimichurri-Beef-Tenderloin">
<h3>Infuse your holiday beef tenderloin with a kick of Latin flavors.</h3>
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>Beef Tenderloin</b><br />
                        5-7 lbs Genuine Texas Beef, Beef Tenderloin
                        <br /><br />
                        <b>Marinade</b><br />
                        1/2 cup fresh parsley, flat leaf<br />
                        1 T kosher salt<br />
                        1/2 cup fresh cilantro<br />
                        1 T fresh chopped garlic<br />
                        1/4 cup fresh oregano leaves<br />
                        1 tsp ground black pepper<br />
                        3/4 cup olive oil<br />
                        1/4 tsp crushed red pepper flakes<br />
                        1/2 cup red wine vinegar<br />
                        Salt and pepper, to taste<br />

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Method</b><br />
Place all marinade ingredients together in a food processor or blender and pulse together until roughly chopped and well blended. Place tenderloin in a shallow dish or large resealable storage bag. Pour mixed marinade over tenderloin and coat thoroughly. Refrigerate for 2 hours to overnight. To cook, remove tenderloin from marinade. Season to taste with salt and pepper. Cook on a hot grill on all sides or in a 425&deg; F oven until desired temperature is reached, using a meat thermometer to check (125&deg; F for medium-rare).               
                        <br /><br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Beef-Tenderloin#Chimichurri-Beef-Tenderloin" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Beef-Tenderloin#Chimichurri-Beef-Tenderloin" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Beef-Tenderloin#Chimichurri-Beef-Tenderloin" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>

        </section>
        
        

        <div class="footer">
          <div class="row">
            <div class="small-12 medium-8 columns">
              <p>
                Copyright &copy; <?php echo date("Y"); ?> Cargill, Incorporated. 151 North Main, Wichita, KS 67202. All Rights Reserved.
              </p>
            </div>
            


          </div>
        </div>  

        <?php /* include($_SERVER['DOCUMENT_ROOT']."/helpers/footer.php"); */ ?>
      </div>
    </div>

    <?php include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/end.php"); ?>
    <script type="text/javascript">
$(document).foundationTabs({deep_linking: true});             
  </script>
   
  </body>
</html>
