<!doctype html>
<?php 
  $pageClass = "idx";
  $pageType = "idx";
  $page2ndType = "idx";
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/config.php"); 
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/head.php"); 
?>  
  </head>


  <body>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">
        <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/nav.php'; ?>
        <a class="exit-off-canvas" href="#"></a>

        <section role="main" id="main">
          <div class="mn_img"></div>
          <div class="row" style="">    
            <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/top_and_nav.php'; ?>
            
            <div class="small-12 medium-7 medium-centered text-center main_content columns">
              <h3>A Tender Tradition Made Easy</h3>
              <p>
                Here in Texas, it&prime;s all about the beef. So why should the holidays be any different? Genuine Texas Beef Prime Rib and Beef Tenderloin are the perfect choices for starting a tender tradition. This season, we invite you to discover how easy it is to prepare a delicious beef prime rib or beef tenderloin, and make it a holiday standard.                
              </p>
            </div>
          </div>
        </section>
        


        <div class="footer">
          <div class="row">
            <div class="small-12 medium-8 columns">
              <p>
                Copyright &copy; <?php echo date("Y"); ?> Cargill, Incorporated. 151 North Main, Wichita, KS 67202. All Rights Reserved.
              </p>
            </div>
            
            <div class="medium-4 columns text-right hide-for-small">
              <img src="<?php echo $img; ?>/MakeItEasyOrn.png" data-at2x="<?php echo $img; ?>/MakeItEasyOrn@2x.png" style="margin-top:-170px;" />
            </div>

          </div>
        </div>  

        <?php /* include($_SERVER['DOCUMENT_ROOT']."/helpers/footer.php"); */ ?>
      </div>
    </div>

    <?php include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/end.php"); ?>
    <script type="text/javascript">
      $(".icn-icoHome").hover( function (e) {
        $(this).toggleClass('icn-icoHome_act', e.type === 'mouseenter');
      });
      $(".icn-icoBeefTen").hover( function (e) {
        $(this).toggleClass('icn-icoBeefTen_act', e.type === 'mouseenter');
      });
      $(".icn-icoPrimeRib").hover( function (e) {
        $(this).toggleClass('icn-icoPrimeRib_act', e.type === 'mouseenter');
      });
      $(".icn-icoSideDish").hover( function (e) {
        $(this).toggleClass('icn-icoSideDish_act', e.type === 'mouseenter');
      });      
      $(".icn-icoCookTip").hover( function (e) {
        $(this).toggleClass('icn-icoCookTip_act', e.type === 'mouseenter');
      });                   
  </script>
   
  </body>
</html>
