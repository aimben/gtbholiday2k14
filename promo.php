<!doctype html>
<?php 
  $pageClass = "promo";
  $pageType = "promo";
  $page2ndType = "promo";
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/config.php"); 


session_start();

if(!isset($_SESSION['count']))
{
   $_SESSION['count'] = 1;
   $_SESSION['first'] = time();
}
else
{
   // Increase the Count
   $_SESSION['count']++;
   // Reset every so often
   if($_SESSION['first'] < (time() - 500))
   {
      $_SESSION['count'] = 1;
      $_SESSION['first'] = time();
   }
   // Die if they have viewed the page too many times
   if($_SESSION['count'] > 2000)
   {
      header('Location: http://genuinetexasbeef.com/Holiday');
   }
}

// OPTIONS - PLEASE CONFIGURE THESE BEFORE USE!
  $yourEmail = "blymer@meetassociated.com"; // the email address you wish to receive these mails through
  $yourWebsite = "GenuineTexasBeef.com/Holiday"; // the name of your website
  $thanksPage = 'coupon.php'; // URL to 'thanks for sending mail' page; leave empty to keep message on the same page 
  $maxPoints = 4; // max points a person can hit before it refuses to submit - recommend 4
  $requiredFields = "fname,lname,entrycode,email,tvshow"; // names of the fields you'd like to be required as a minimum, separate each field with a comma


  // DO NOT EDIT BELOW HERE
  $error_msg = array();
  $result = null;

  $requiredFields = explode(",", $requiredFields);

  function clean($data) {
    $data = trim(stripslashes(strip_tags($data)));
    return $data;
  }
  function isBot() {
    $bots = array("Indy", "Blaiz", "Java", "libwww-perl", "Python", "OutfoxBot", "User-Agent", "PycURL", "AlphaServer", "T8Abot", "Syntryx", "WinHttp", "WebBandit", "nicebot", "Teoma", "alexa", "froogle", "inktomi", "looksmart", "URL_Spider_SQL", "Firefly", "NationalDirectory", "Ask Jeeves", "TECNOSEEK", "InfoSeek", "WebFindBot", "girafabot", "crawler", "www.galaxy.com", "Googlebot", "Scooter", "Slurp", "appie", "FAST", "WebBug", "Spade", "ZyBorg", "rabaz");

    foreach ($bots as $bot)
      if (stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false)
        return true;

    if (empty($_SERVER['HTTP_USER_AGENT']) || $_SERVER['HTTP_USER_AGENT'] == " ")
      return true;
    
    return false;
  }

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    if (isBot() !== false)
      $error_msg[] = "No bots please! UA reported as: ".$_SERVER['HTTP_USER_AGENT'];
      
    // lets check a few things - not enough to trigger an error on their own, but worth assigning a spam score.. 
    // score quickly adds up therefore allowing genuine users with 'accidental' score through but cutting out real spam :)
    $points = (int)0;
    
    $badwords = array("adult", "beastial", "bestial", "blowjob", "clit", "cum", "cunilingus", "cunillingus", "cunnilingus", "cunt", "ejaculate", "fag", "felatio", "fellatio", "fuck", "fuk", "fuks", "gangbang", "gangbanged", "gangbangs", "hotsex", "hardcode", "jism", "jiz", "orgasim", "orgasims", "orgasm", "orgasms", "phonesex", "phuk", "phuq", "pussies", "pussy", "spunk", "xxx", "viagra", "phentermine", "tramadol", "adipex", "advai", "alprazolam", "ambien", "ambian", "amoxicillin", "antivert", "blackjack", "backgammon", "texas", "holdem", "poker", "carisoprodol", "ciara", "ciprofloxacin", "debt", "dating", "porn", "link=", "voyeur", "content-type", "bcc:", "cc:", "document.cookie", "onclick", "onload", "javascript");

    foreach ($badwords as $word)
      if (
        strpos(strtolower($_POST['fname']), $word) !== false || 
        strpos(strtolower($_POST['lname']), $word) !== false
      )
        $points += 2;
    
    if (isset($_POST['nojs']))
      $points += 1;
    if (strlen($_POST['fname']) < 3)
      $points += 1;
    if (strlen($_POST['lname']) < 3)
      $points += 1;
    // end score assignments

    foreach($requiredFields as $field) {
      trim($_POST[$field]);
      
      if (!isset($_POST[$field]) || empty($_POST[$field]) && array_pop($error_msg) != "Please fill in all the required fields and submit again.\r\n")
        $error_msg[] = "Please fill in all the required fields and submit again.";
    }

    if (!empty($_POST['fname']) && !preg_match("/^[a-zA-Z-'\s]*$/", stripslashes($_POST['fname'])))
      $error_msg[] = "The name field must not contain special characters.\r\n";
    if (!empty($_POST['lname']) && !preg_match("/^[a-zA-Z-'\s]*$/", stripslashes($_POST['lname'])))
      $error_msg[] = "The name field must not contain special characters.\r\n";
    if (!empty($_POST['email']) && !preg_match('/^([a-z0-9])(([-a-z0-9._])*([a-z0-9]))*\@([a-z0-9])(([a-z0-9-])*([a-z0-9]))+' . '(\.([a-z0-9])([-a-z0-9_-])?([a-z0-9])+)+$/i', strtolower($_POST['email'])))
      $error_msg[] = "That is not a valid e-mail address.\r\n";

    if ($error_msg == NULL && $points <= $maxPoints) {
      /*
      $subject = "Automatic Form Email";
      
      $message = "You received this e-mail message through your website (GenuineTexasBeef.com/Holidays): \n\n";
      foreach ($_POST as $key => $val) {
        if (is_array($val)) {
          foreach ($val as $subval) {
            $message .= ucwords($key) . ": " . clean($subval) . "\r\n";
          }
        } else {
          $message .= ucwords($key) . ": " . clean($val) . "\r\n";
        }
      }
      $message .= "\r\n";
      $message .= 'IP: '.$_SERVER['REMOTE_ADDR']."\r\n";
      $message .= 'Browser: '.$_SERVER['HTTP_USER_AGENT']."\r\n";
      $message .= 'Points: '.$points;

      if (strstr($_SERVER['SERVER_SOFTWARE'], "Win")) {
        $headers   = "From: $yourEmail\r\n";
      } else {
        $headers   = "From: $yourWebsite <$yourEmail>\r\n"; 
      }
      $headers  .= "Reply-To: {$_POST['email']}\r\n";
      */

      /* Change db and connect values if using online */
      $db="associated_clients";
      $link = mysql_connect('mysql.associatedanalytics.com', 'aim_client_wrt' , 'associ8tedclientread');
      if (! $link) die(mysql_error());
      mysql_select_db($db , $link) or die("Select Error: ".mysql_error());
      $result=mysql_query(
      "INSERT INTO gtb_test_holiday (
      fname,
      lname,
      entrycode,
      email,
      tvshow) VALUES (
      '$fname',
      '$lname',
      '$entrycode',
      '$email',
      '$tvshow')") or die("Insert Error: ".mysql_error());

      mysql_close($link);

      if (mail($yourEmail,$subject,$message,$headers)) {
        if (!empty($thanksPage)) {
          header("Location: $thanksPage");
          exit;
        } else {
          $result = 'Your mail was successfully sent.';
          $disable = true;
        }
      } else {
        $error_msg[] = 'Your mail could not be sent this time. ['.$points.']';
      }
    } else {
      if (empty($error_msg))
        $error_msg[] = 'Your mail looks too much like spam, and could not be sent this time. ['.$points.']';
    }
  }
  function get_data($var) {
    if (isset($_POST[$var]))
      echo htmlspecialchars($_POST[$var]);
  }

  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/head.php"); 
?>  
  </head>


  <body>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">
        <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/nav.php'; ?>
        <a class="exit-off-canvas" href="#"></a>

        <section role="main" id="main">
          <div class="row hide-for-small">
            <div class="medium-12 columns" style="margin-top:-50px;">
              <div class="" style="position:absolute; top:0; right:20px;">
                <img src="<?php echo $img; ?>/MakeItEasyOrn.png" data-at2x="<?php echo $img; ?>/MakeItEasyOrn@2x.png" style="" />
              </div>
            </div>
          </div>

          <div class="mn_img"></div>
          <div class="row" style="">    
            <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/top_and_nav.php'; ?>
            
            <div class="small-12 main_content columns">
              <div class="row">
                <div class="small-12 medium-7 columns">
                  <h3>A Tender Tradition Made Easy</h3>
                  <p>
                    Here in Texas, it&prime;s all about the beef. So why should the holidays be any different? Genuine Texas Beef Prime Rib and Beef Tenderloin are the perfect choices for starting a tender tradition. This season, we invite you to discover how easy it is to prepare a delicious beef prime rib or beef tenderloin, and make it a holiday standard.                
                  </p>
                </div>
 
                <div class="small-12 medium-4 columns" style="background:white; border-radius:8px; padding:; color:black">
                <p>
Did you see us on <b>Trends and Friends</b> or <b>Studio 4</b>? Enter the unique code mentioned on the show for savings on Genuine Texas Beef products.
</p>                  



<?php
                if (!empty($error_msg)) {
                  echo '<p class="error">ERROR: '. implode("<br />", $error_msg) . "</p>";
                }
                if ($result != NULL) {
                  echo '<p class="success">'. $result . "</p>";
                }
                ?>
                <form action="<?php echo basename(__FILE__); ?>" method="post">
                  <noscript>
                    <p><input type="hidden" name="nojs" id="nojs" /></p>
                  </noscript>
                  <div class="row">
                    <div class="large-6 columns">
                      <input type="text" name="fname" id="fname" value="<?php get_data("fname"); ?>" placeholder="First Name" />                      
                    </div>

                    <div class="large-6 columns">
                      <input type="text" name="lname" id="lname" value="<?php get_data("lname"); ?>" placeholder="Last Name" />                      
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-6 columns">
                      <input type="text" name="email" id="email" value="<?php get_data("email"); ?>" placeholder="Email" />                    
                    </div>

                    <div class="large-6 columns">
                      <input type="text" name="entrycode" id="entrycode" value="<?php get_data("entrycode"); ?>" placeholder="Entry Code" />                      
                    </div>
                  </div>

                  <div class="row">
                    <div class="large-6 columns">
                        <select name="tvshow" id="tvshow" value="<?php get_data("tvshow"); ?>" size="1" style="width:;" required>
                            <option value="" selected>TV Segment:</option>
                            <option value="Trends and Friends" >Trends and Friends</option>
                            <option value="Studio 4" >Studio 4</option>
                        </select>
                      </div>

                      <div class="large-6 columns">
                        <input type="image" src="" class="icoSubmit text-left" value="Submit" id="submit" value="" <?php if (isset($disable) && $disable === true) echo ' disabled="disabled"'; ?> >
                      </div>
                    </div>
                </form>



                </div>
              </div>
            </div>


          </div>
        </section>
        


        <div class="footer">
          <div class="row">
            <div class="small-12 medium-6 columns">
              <p>
                Copyright &copy; <?php echo date("Y"); ?> Cargill, Incorporated. 151 North Main, Wichita, KS 67202. All Rights Reserved.
              </p>
            </div>

            <div class="small-12 medium-6 hide-for-small social text-right columns" style="z-index:9999">
              <ul class="inline-list text-right" style="z-index:">
                <li><a href="#" class="st_pinterest_custom fa fa-pinterest-square" style="z-index:"></a></li>
                <li><a href="#" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                <li><a href="#" class="st_facebook_custom fa fa-facebook-square"></a></li>                
                <li class="icoShareBTN" style=""></li>
              </ul>
            </div>

          </div>
        </div>  

        <?php /* include($_SERVER['DOCUMENT_ROOT']."/helpers/footer.php"); */ ?>
      </div>
    </div>

    <?php include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/end.php"); ?>
    <script type="text/javascript">
      $(".icn-icoHome").hover( function (e) {
        $(this).toggleClass('icn-icoHome_act', e.type === 'mouseenter');
      });
      $(".icn-icoBeefTen").hover( function (e) {
        $(this).toggleClass('icn-icoBeefTen_act', e.type === 'mouseenter');
      });
      $(".icn-icoPrimeRib").hover( function (e) {
        $(this).toggleClass('icn-icoPrimeRib_act', e.type === 'mouseenter');
      });
      $(".icn-icoSideDish").hover( function (e) {
        $(this).toggleClass('icn-icoSideDish_act', e.type === 'mouseenter');
      });      
      $(".icn-icoCookTip").hover( function (e) {
        $(this).toggleClass('icn-icoCookTip_act', e.type === 'mouseenter');
      });                   
  </script>
   
  </body>
</html>
