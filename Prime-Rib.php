<!doctype html>
<?php 
  $pageClass = "prime-rib recipe_pg";
  $pageType = "prime-rib";
  $page2ndType = "prime-rib";
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/config.php"); 
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/head.php"); 
?>  
  </head>


  <body>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">
        <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/nav.php'; ?>
        <a class="exit-off-canvas" href="#"></a>

        <section role="main" id="main">
          <div class="mn_img"></div>
          <div class="row" style="">    
            <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/top_and_nav.php'; ?>
            
            <div class="small-12 medium-8 medium-centered text-center main_content columns">
              <h3>A Tradition for the Most Special Occasions</h3>
              <p>
               As the best roast money can buy, a beef prime rib is the perfect cut for the most special occasions. Choose from one of our delicious beef prime rib recipes and don&prime;t forget to utilize our easy cooking tips. The result is going to be tender, juicy and delicious. 
              </p>
            </div>
          </div>

          <div class="recipes">
            <div class="row">
              <div class="small-12 columns">
                <dl class="tabs vertical" data-tab data-options="deep_linking:true">
                  <dd class="active"><a href="#Garlic-Rosemary-Beef-Prime-Rib">Garlic Rosemary Beef Prime Rib</a></dd>
                  <dd><a href="#Horseradish-Rub-Beef-Prime-Rib">Horseradish Rub Beef Prime Rib</a></dd>
                </dl>
                <div class="tabs-content" style="">
                  <div class="content active" id="Garlic-Rosemary-Beef-Prime-Rib" style="">
                    <h3>The earthy flavors in this dish will leave your guests wanting seconds.</h3>
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>Beef Prime Rib</b><br />
                        4-6 lbs Genuine Texas Beef, Boneless Beef Prime Rib Roast
                        <br /><br />
                        <b>Rub</b><br />
                        2 tsp freshly ground pepper<br />
                        1 T seasoning salt<br />
                        1 T fresh chopped garlic<br />
                        2 T dried rosemary<br />

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Method</b><br />
Preheat oven to 350&deg; F. Rub the seasoning mixture over the entire outside of the roast. Place in a roasting pan, fat side up, and cook uncovered until it reaches desired internal temperature, using a meat thermometer to check (125&deg; F for medium-rare). Let roast stand for 30 minutes before slicing in order to let the juices absorb into the meat.         
                        <br /><br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Prime-Rib#Garlic-Rosemary" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Prime-Rib#Garlic-Rosemary" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Prime-Rib#Garlic-Rosemary" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>
                  </div>

                  <div class="content" id="Horseradish-Rub-Beef-Prime-Rib">
<h3>Prepare a classic beef prime rib your guests are sure to love.</h3>
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>Beef Prime Rib</b><br />
                        5-6 lbs Genuine Texas Beef, Boneless Beef Prime Rib Roast

                        <br /><br />
                        <b>Rub</b><br />
                        1/4 cup olive oil<br />
                        1 T kosher salt<br />
                        1/3 cup horseradish, grated creamy-style<br />
                        3 T fresh thyme, chopped<br />
                        1/4 cup garlic, chopped<br />
                        Cracked pepper, to taste<br />

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Method</b><br />
Preheat oven to 350&deg; F. Place all ingredients for rub in a food processor and pulse until smooth. Rub mixture over the entire outside of the roast. Place in a roasting pan, fat side up, and cook uncovered until it reaches desired internal temperature, using a meat thermometer to check (125&deg; F for medium-rare, about 2 hours). Let roast stand for 30 minutes before slicing in order to let the juices absorb into the meat.             
                        <br /><br /><br />
                        <div class="social2">
                          <ul class="inline-list text-right" style="z-index:">
                            <li>Share</li>
                            <li><a href="#" st_url="<?php echo $url; ?>/Prime-Rib#Horseradish-Rub" class="st_facebook_custom fa fa-facebook-square"></a></li>
                            <li><a href="#" st_url="<?php echo $url; ?>/Prime-Rib#Horseradish-Rub" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                            <li><a href="#" st_url="<?php echo $url; ?>/Prime-Rib#Horseradish-Rub" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
          
                          </ul>
                        </div>

                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>

        </section>
        
        

        <div class="footer">
          <div class="row">
            <div class="small-12 medium-8 columns">
              <p>
                Copyright &copy; <?php echo date("Y"); ?> Cargill, Incorporated. 151 North Main, Wichita, KS 67202. All Rights Reserved.
              </p>
            </div>
            


          </div>
        </div>  

        <?php /* include($_SERVER['DOCUMENT_ROOT']."/helpers/footer.php"); */ ?>
      </div>
    </div>

    <?php include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/end.php"); ?>
    <script type="text/javascript">
$(document).foundationTabs({deep_linking: true});             
  </script>
   
  </body>
</html>
