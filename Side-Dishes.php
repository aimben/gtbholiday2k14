<!doctype html>
<?php 
  $pageClass = "side-dish recipe_pg";
  $pageType = "side-dish";
  $page2ndType = "side-dish";
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/config.php"); 
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/head.php"); 
?>  
  </head>


  <body>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">
        <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/nav.php'; ?>
        <a class="exit-off-canvas" href="#"></a>

        <section role="main" id="main">
          <div class="mn_img"></div>
          
          <div class="row" style="">    
            <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/top_and_nav.php'; ?>
            
            <div class="small-12 medium-7 medium-centered text-center main_content columns">
              <h3>Side Dishes Worthy of a Celebration</h3>
              <p>
               During the holiday season, it&prime;s all about friends and family. Choose side dishes they&prime;ll love and that complement your tender, juicy and flavorful Genuine Texas Beef Prime Rib or Beef Tenderloin.
              </p>
            </div>
          </div>

          <div class="recipes">
            <div class="row">
              <div class="small-12 columns">
                <dl class="tabs vertical" data-tab data-options="deep_linking:true">
                  <dd class="active"><a href="#Green-Bean-Casserole">Green Bean Casserole</a></dd>
                  <dd><a href="#Stuffed-Sweet-Potatoes">Stuffed Sweet Potatoes</a></dd>
                  <dd><a href="#Cranberry-Citrus-Sauce">Cranberry Citrus Sauce</a></dd>
                </dl>
                <div class="tabs-content" style="">
                  <div class="content active" id="Green-Bean-Casserole" style="">
                    <h3>Serves: 4-6</h3>
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>Toppings</b><br />
2 medium onions, thinly sliced<br />
1/4 cup all-purpose flour<br />
2 T bread crumbs<br />
1 tsp kosher salt<br />
Nonstick cooking spray<br />

                        <br /><br />
                        <b>Sauce</b><br />
2 T plus 1 tsp kosher salt, divided<br />
1 lb fresh green beans, rinsed, trimmed and halved<br />
2 T unsalted butter<br />
12 oz mushrooms, trimmed and cut into 1/2-inch pieces<br />
1/2 tsp freshly ground black pepper<br />
2 cloves garlic, minced<br />
1/4 tsp freshly ground nutmeg<br />
2 T all-purpose flour<br />
1 cup chicken broth<br />
1 cup half-and-half<br />

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Method</b><br />
Preheat the oven to 475&deg; F.
Combine the onions, flour, bread crumbs and salt in a large mixing bowl and toss to combine. Coat a sheet pan with nonstick cooking spray and evenly spread the onions on the pan. Place the pan on the middle rack of the oven and bake until golden brown, approximately 30 minutes. Toss the onions 2 to 3 times during cooking. Once done, remove from the oven and set aside until ready to use. Turn the oven down to 400&deg; F.
<br /><br />
While the onions are cooking, prepare the beans. Bring a gallon of water and 2 tablespoons of salt to a boil in an 8-quart saucepan. Add the beans and blanch for 5 minutes. Drain in a colander and immediately plunge the beans into a large bowl of ice water to stop the cooking. Drain and set aside.
<br /><br />
Melt the butter in a 12-inch cast iron skillet over medium-high heat. Add the mushrooms, 1 teaspoon salt and pepper and cook, stirring occasionally, until the mushrooms begin to give up some of their liquid, approximately 4 to 5 minutes. Add the garlic and nutmeg and continue to cook for another 1 to 2 minutes. Sprinkle the flour over the mixture and stir to combine. Cook for 1 minute.
<br /><br />
Add the broth and simmer for 1 minute. Decrease the heat to medium-low and add the half-and-half. Cook until the mixture thickens, stirring occasionally, approximately 6 to 8 minutes. Remove from the heat and stir in 1/4 of the onions and all of the green beans. Top with the remaining onions. Place into the oven and bake until bubbly, approximately 15 minutes. Remove and serve immediately.            
                        <br /><br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Green-Bean-Casserole" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Green-Bean-Casserole" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Green-Bean-Casserole" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>
                  </div>

                  <div class="content" id="Stuffed-Sweet-Potatoes">
<h3>Serves: 12</h3>
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>INGREDIENTS</b><br />
12 large sweet potatoes<br />
3/4 cup unsalted butter, at room temperature<br />
3/4 cup light brown sugar<br />
3/4 cup all-purpose flour<br />
1/4 tsp ground cinnamon<br />
1/4 tsp salt<br />
1 cup toasted pecan pieces<br />
1 cup miniature marshmallows

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Method</b><br />
Preheat the oven to 400&deg; F. Wash the sweet potatoes, scrubbing them well to remove any dirt. With a fork, prick the sweet potatoes in a couple of spots and place them on a sheet pan. Bake for about 45 minutes, or until a knife inserted in the center goes in easily. In a large bowl, mix the butter, brown sugar and flour together until crumbly. Add the cinnamon, salt, pecans and marshmallows; fold the streusel topping together to combine. Slice each sweet potato lengthwise down the center and push the ends toward the middle so it opens up. Stuff the sweet potatoes generously with the streusel topping and return to the oven. Bake for 20 minutes or until the topping is bubbling and brown.        
                        <br /><br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Stuffed-Sweet-Potatoes" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Stuffed-Sweet-Potatoes" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Stuffed-Sweet-Potatoes" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>                    
                  </div>

<div class="content" id="Cranberry-Citrus-Sauce">
<h3>Yields: 7&ndash;1/2 cups</h3>
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>INGREDIENTS</b><br />
3 cups sugar<br />
1-1/2 cups water<br />
2 vanilla beans, split lengthwise<br />
1 orange, zested and juiced<br />
3 bags (12 oz ea) fresh or frozen cranberries<br />
Salt, to taste<br />
Freshly ground black pepper, to taste<br />
3 T Dijon mustard

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Method</b><br />
Start by making a simple syrup: dissolve the sugar in the water in a medium saucepan. Add vanilla beans and bring to a simmer. Add the orange juice and cranberries, a dash of salt and some freshly ground black pepper, to taste. Simmer until cranberries begin to pop and become tender, about 3 to 5 minutes. Remove from heat and stir in Dijon mustard. Chill cranberry sauce until ready to serve. The sauce can be made up to 2 days ahead of time. Just before serving, garnish sauce with freshly grated orange zest.
                        <br /><br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Cranberry-Citrus-Sauce" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Cranberry-Citrus-Sauce" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Side-Dishes#Cranberry-Citrus-Sauce" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>                    
                  </div>

                </div>
              </div>
            </div>
          </div>

        </section>
        
        

        <div class="footer">
          <div class="row">
            <div class="small-12 medium-8 columns">
              <p>
                Copyright &copy; <?php echo date("Y"); ?> Cargill, Incorporated. 151 North Main, Wichita, KS 67202. All Rights Reserved.
              </p>
            </div>
            


          </div>
        </div>  

        <?php /* include($_SERVER['DOCUMENT_ROOT']."/helpers/footer.php"); */ ?>
      </div>
    </div>

    <?php include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/end.php"); ?>
    <script type="text/javascript">
$(document).foundationTabs({deep_linking: true});             
  </script>
   
  </body>
</html>
