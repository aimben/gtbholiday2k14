<div class="medium-12 text-center hide-for-small columns" style="margin-top:-55px; z-index:2">
  <a href="<?php echo $url; ?>" class="button_hv float-shadow"><span class="icoHome <?php if ($pageType == "idx") echo 'active'; ?>"></span></a>
  <a href="<?php echo $url; ?>/Beef-Tenderloin" class="button_hv float-shadow"><span class="icoBeefTen <?php if ($pageType == "beef-tenderloin") echo 'active'; ?>"></span></a>
  <a href="<?php echo $url; ?>/Prime-Rib" class="button_hv float-shadow"><span class="icoPrimeRib <?php if ($pageType == "prime-rib") echo 'active'; ?>"></span></a>
  <a href="<?php echo $url; ?>/Side-Dishes" class="button_hv float-shadow"><span class="icoSideDish <?php if ($pageType == "side-dish") echo 'active'; ?>"></span></a>
  <a href="<?php echo $url; ?>/Cooking-Tips" class="button_hv float-shadow"><span class="icoCookTip <?php if ($pageType == "cook-tips") echo 'active'; ?>"></span></a>
</div>

<?php if ($pageType == "promo"): ?>

<?php else: ?>
  <div class="large-1 columns"></div>
  <div class="medium-3 medium-offset-9 hide-for-small social text-right columns" style="z-index:9999">
    <ul class="inline-list text-right" style="z-index:">
      <li><a href="#" class="st_pinterest_custom fa fa-pinterest-square" style="z-index:"></a></li>
      <li><a href="#" class="st_twitter_custom fa fa-twitter-square"></a></li>                
      <li><a href="#" class="st_facebook_custom fa fa-facebook-square"></a></li>                
      <li class="icoShareBTN" style=""></li>
    </ul>
  </div>
<?php endif ?>