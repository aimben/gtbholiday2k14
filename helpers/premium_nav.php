<?php if ($pageType == "retail"): ?>
  <div class="small-11 medium-8 large-6 small-centered nav_badges_pr columns">
    <div class="row">
      <div class="small-6 text-right columns" style="">
        <a href="#MarketingSupport" title="Marketing Support" class="button_hv float-shadow">
          <img src="<?php echo $img; ?>/page_nav/Retail_Marketing.png" alt="Marketing Support" data-at2x="<?php echo $img; ?>/page_nav/Retail_Marketing@2x.png" class="hide-for-small" />
          <img src="<?php echo $img; ?>/page_nav/Mob_MarketingSupport.png" alt="Marketing Support" data-at2x="<?php echo $img; ?>/page_nav/Mob_MarketingSupport@2x.png" class="show-for-small" />
          <h5>Marketing Support</h5>
        </a>
      </div>

      <div class="small-6 text-left columns end">
        <a href="<?php echo $url; ?>/" title="Resources Login" class="button_hv float-shadow"> 
          <img src="<?php echo $img; ?>/page_nav/Foodservice_Resources_Login.png" alt="Resources Login" data-at2x="<?php echo $img; ?>/page_nav/Foodservice_Resources_Login@2x.png" class="hide-for-small" />
          <img src="<?php echo $img; ?>/page_nav/Mob_ResourcesLogin.png" alt="Resources Login" data-at2x="<?php echo $img; ?>/page_nav/Mob_ResourcesLogin@2x.png" class="show-for-small" />
          <h5>Resources Login</h5>
        </a>
      </div>                                                      
    </div>
  </div>  

<?php else: ?>   
  <div class="small-11 medium-8 large-6 small-centered nav_badges_pr columns">
    <div class="row">
      <div class="small-3 columns" style="">
        <?php if ($pageType == "beef"): ?>
          <a href="<?php echo $url; ?>/Premium-Beef/Recipes/" title="Beef Recipes" class="<?php if ($page3rdType == "beef_recipes") echo 'active'; ?> button_hv float-shadow">
            <img src="<?php echo $img; ?>/page_nav/Beef_Recipes_Icon.png" alt="Beef Recipes" data-at2x="<?php echo $img; ?>/page_nav/Beef_Recipes_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Beef_Recipes.png" alt="Beef Recipes" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Beef_Recipes@2x.png" class="show-for-small" />
            <h5>Beef Recipes</h5>
          </a>

        <?php elseif ($pageType == "pork"): ?>
          <a href="<?php echo $url; ?>/Premium-Pork/Recipes/" title="Pork Recipes" class="<?php if ($page3rdType == "pork_recipes") echo 'active'; ?> button_hv float-shadow">
            <img src="<?php echo $img; ?>/page_nav/Pork_Recipes_Icon.png" alt="Pork Recipes" data-at2x="<?php echo $img; ?>/page_nav/Pork_Recipes_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Pork_Recipes.png" alt="Pork Recipes" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Pork_Recipes@2x.png" class="show-for-small" />
            <h5>Pork Recipes</h5>
          </a>    

        <?php elseif ($pageType == "foodservice"): ?>  
          <a href="<?php echo $url; ?>/" title="Foodservice Training" class="button_hv float-shadow">
            <img src="<?php echo $img; ?>/page_nav/Foodservice_Training.png" alt="Foodservice Training" data-at2x="<?php echo $img; ?>/page_nav/Foodservice_Training@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mob_Training.png" alt="Foodservice Training" data-at2x="<?php echo $img; ?>/page_nav/Mob_Training@2x.png" class="show-for-small" />
            <h5>Foodservice Training</h5>
          </a>        
        <?php else: ?> 
        <?php endif ?>           
      </div>

      <div class="small-3 columns">
        <?php if ($pageType == "beef"): ?>
          <a href="<?php echo $url; ?>/Premium-Beef/Tips" title="Beef Tips" class="<?php if ($page3rdType == "beef_tips") echo 'active'; ?> button_hv float-shadow">
            <img src="<?php echo $img; ?>/page_nav/Beef_Tips_Icon.png" alt="Beef Tips" data-at2x="<?php echo $img; ?>/page_nav/Beef_Tips_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Beef_Tips.png" alt="Beef Tips" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Beef_Tips@2x.png" class="show-for-small" />
            <h5>Beef Tips</h5>
          </a>

        <?php elseif ($pageType == "pork"): ?>
          <a href="<?php echo $url; ?>/Premium-Pork/Tips" title="Pork Tips" class="<?php if ($page3rdType == "pork_tips") echo 'active'; ?> button_hv float-shadow"> 
            <img src="<?php echo $img; ?>/page_nav/Pork_Tips_Icon.png" alt="Pork Tips" data-at2x="<?php echo $img; ?>/page_nav/Pork_Tips_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Pork_Tips.png" alt="Pork Tips" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Pork_Tips@2x.png" class="show-for-small" />
            <h5>Pork Tips</h5>
          </a>        

        <?php elseif ($pageType == "foodservice"): ?>  
          <a href="<?php echo $url; ?>/" title="Foodservice Recipes" class="button_hv float-shadow"> 
            <img src="<?php echo $img; ?>/page_nav/Foodservice_Recipes.png" alt="Foodservice Recipes" data-at2x="<?php echo $img; ?>/page_nav/Foodservice_Recipes@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mob_FoodserviceRecipes.png" alt="Foodservice Recipes" data-at2x="<?php echo $img; ?>/page_nav/Mob_FoodserviceRecipes@2x.png" class="show-for-small" />
            <h5>Foodservice Recipes</h5>
          </a>        
        <?php else: ?> 
        <?php endif ?>
      </div>

      <div class="small-3 columns">
        <?php if ($pageType == "beef"): ?>
          <a href="<?php echo $url; ?>/Premium-Beef/Nutrition" title="Beef Nutrition" class="<?php if ($page3rdType == "beef_nutrition") echo 'active'; ?> button_hv float-shadow">
            <img src="<?php echo $img; ?>/page_nav/Beef_Nutrition_Icon.png" alt="Beef Nutrition" data-at2x="<?php echo $img; ?>/page_nav/Beef_Nutrition_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Beef_Nutrition.png" alt="Beef Nutrition" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Beef_Nutrition@2x.png" class="show-for-small" />
            <h5>Beef Nutrition</h5>
          </a>

        <?php elseif ($pageType == "pork"): ?>
          <a href="<?php echo $url; ?>/Premium-Pork/Nutrition" title="Pork Nutrition" class="<?php if ($page3rdType == "pork_nutrition") echo 'active'; ?> button_hv float-shadow"> 
            <img src="<?php echo $img; ?>/page_nav/Pork_Nutrition_Icon.png" alt="Pork Nutrition" data-at2x="<?php echo $img; ?>/page_nav/Pork_Nutrition_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Pork_Nutrition.png" alt="Pork Nutrition" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Pork_Nutrition@2x.png" class="show-for-small" />
            <h5>Pork Nutrition</h5>
          </a>        

        <?php elseif ($pageType == "foodservice"): ?>  
          <a href="<?php echo $url; ?>/" title="Foodservice Distributors" class="button_hv float-shadow"> 
            <img src="<?php echo $img; ?>/page_nav/Foodservice_Distributor.png" alt="Foodservice Distributors" data-at2x="<?php echo $img; ?>/page_nav/Foodservice_Distributor@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mob_FindADistributor.png" alt="Foodservice Distributors" data-at2x="<?php echo $img; ?>/page_nav/Mob_FindADistributor@2x.png" class="show-for-small" />
            <h5>Foodservice Distributors</h5>
          </a>        
        <?php else: ?> 
        <?php endif ?>
      </div>

      <div class="small-3 columns end">
        <?php if ($pageType == "beef"): ?>
          <a href="<?php echo $url; ?>/Premium-Beef/Cut-Info" title="Beef Cut Info" class="<?php if ($page3rdType == "beef_cuts") echo 'active'; ?> button_hv float-shadow">
            <img src="<?php echo $img; ?>/page_nav/Beef_Cut_Icon.png" alt="Beef Cut Info" data-at2x="<?php echo $img; ?>/page_nav/Beef_Cut_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Beef_Cut_Info.png" alt="Beef Cut Info" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Beef_Cut_Info@2x.png" class="show-for-small" />
            <h5>Beef Cut Info</h5>
          </a>

        <?php elseif ($pageType == "pork"): ?>
          <a href="<?php echo $url; ?>/Premium-Pork/Cut-Info" title="Pork Cut Info" class="<?php if ($page3rdType == "pork_cuts") echo 'active'; ?> button_hv float-shadow"> 
            <img src="<?php echo $img; ?>/page_nav/Pork_Cut_Icon.png" alt="Pork Cut Info" data-at2x="<?php echo $img; ?>/page_nav/Pork_Cut_Icon@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mobile_Pork_Cut_Info.png" alt="Pork Cut Info" data-at2x="<?php echo $img; ?>/page_nav/Mobile_Pork_Cut_Info@2x.png" class="show-for-small" />
            <h5>Pork Cut Info</h5>
          </a>        

        <?php elseif ($pageType == "foodservice"): ?>  
          <a href="<?php echo $url; ?>/" title="Foodservice Resources" class="button_hv float-shadow"> 
            <img src="<?php echo $img; ?>/page_nav/Foodservice_Resources_Login.png" alt="Foodservice Resources" data-at2x="<?php echo $img; ?>/page_nav/Foodservice_Resources_Login@2x.png" class="hide-for-small" />
            <img src="<?php echo $img; ?>/page_nav/Mob_ResourcesLogin.png" alt="Foodservice Resources" data-at2x="<?php echo $img; ?>/page_nav/Mob_ResourcesLogin@2x.png" class="show-for-small" />
            <h5>Foodservice Resources</h5>
          </a>        
        <?php else: ?> 
        <?php endif ?>
      </div>                                                      
    </div>
  </div>
<?php endif ?>     