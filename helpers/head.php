<!--[if lte IE 8]><html class="no-js lt-ie9 <?php echo $pageClass ?>" lang="en" version="HTML+RDFa 1.1"><![endif]-->
<!--[if IE 9]><html class="no-js ie9 <?php echo $pageClass ?>" lang="en" version="HTML+RDFa 1.1"><![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js <?php echo $pageClass ?>" lang="en" version="HTML+RDFa 1.1"><!--<![endif]-->

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<title><?php echo $pageTitle; ?></title>

	<meta name='keywords' content='<?php echo $pageKeywords; ?>' />
	<meta name='description' content='<?php echo $pageDescription; ?>' />
	<meta name="robots" content="index, follow">
	<meta name="author" content="Sterling Silver Meats">
	
	<meta property="og:title" content="<?php echo $pageFBtitle; ?>"/>
	<meta property="og:url" content="<?php echo $pageURL; ?>"/>
	<meta property="og:image" content="<?php echo $pageImage; ?>"/>
	<meta property="og:description" content="<?php echo $pageFBdescription; ?>"/>


    <link rel="stylesheet" href="<?php echo "http://".$_SERVER['HTTP_HOST']; ?>/Holiday/css/app.css" />
    <script src="<?php echo "http://".$_SERVER['HTTP_HOST']; ?>/Holiday/js/modernizr/modernizr.js"></script>	
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	
	<?php /* link rel="shortcut icon" href="<?php echo "http://".$_SERVER['HTTP_HOST']; ?>/favicon.ico" */ ?>

	<script type="text/javascript">var switchTo5x=true;</script>

	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "ec3228de-8661-4e2a-9985-b506e98db2bd", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

	<?php include ($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/gFonts.php"); ?>

	<!--[if lt IE 9]>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
	  <script src="//s3.amazonaws.com/nwapi/nwmatcher/nwmatcher-1.2.5-min.js"></script>
	  <script src="//html5base.googlecode.com/svn-history/r38/trunk/js/selectivizr-1.0.3b.js"></script>
	  <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.1.0/respond.min.js"></script>
	<![endif]-->	
	
	<!--[if IE 9]>
	<script type="text/javascript">
		if (!window.console) {
		    window.console = {log: function() {}};
		}
	</script>
	<![endif]-->
	