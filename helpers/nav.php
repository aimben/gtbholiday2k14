<div class="head_repeat" style="">
  <nav class="tab-bar">
    <section class="left tab-bar-section">
      <div class="row">    
        <div class="small-12 text-left logo_cont columns">
          <a id="sslogo" href="<?php echo $url; ?>">
            <img src="<?php echo $img; ?>/gtb_logo.png" data-at2x="<?php echo $img; ?>/gtb_logo@2x.png" />
          </a>
         
        </div>            
      </div>
    </section>

    <section class="right-small hide-for-medium-up">
      <a class="right-off-canvas-toggle menu-icon" href="#"><span></span></a>
    </section>
  </nav>

  <aside class="right-off-canvas-menu hide-for-medium-up" style="z-index:99999">
    <ul class="off-canvas-list">
      <li><a href="<?php echo $url; ?>/">Home</a></li>
      <li><a href="<?php echo $url; ?>/Beef-Tenderloin">Tenderloin</a></li>
      <li><a href="<?php echo $url; ?>/Prime-Rib">Prime Rib</a></li>
      <li><a href="<?php echo $url; ?>/Side-Dishes">Side Dishes</a></li>
      <li><a href="<?php echo $url; ?>/Cooking-Tips">Cooking Tips</a></li>

    </ul>
  </aside>

  
</div>