        <div class="footer" style="">
          <div class="row">
            <div class="small-12 large-6 newslett columns" style="">
              <div class="row">
                <div class="small-1 columns">
                  <a href="#" class="icon-ico-mail mail_wrd"></a>
                </div>
                <div class="small-10 large-11 columns">
                  <span class="break">Get Special Offers, News and More</span>
                  <span>Join our exclusive mailing list today!</span>
                </div>
              </div>  
            </div>

            <div class="small-12 large-6 social columns">
              <ul class="listStyle-inline" style="">
                <li><a href="#" class="icon-ico-fb facebook_wrd"></a></li>
                <li><a href="#" class="icon-ico-twitter twitter_wrd"></a></li>
                <li><a href="#" class="icon-ico-tube tube_wrd"></a></li>

              </ul>
            </div>
          </div>
        </div>