  <?php /* <script src="js/main.min.js"></script> */ ?>
  <script src="<?php echo $url; ?>/js/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo $url; ?>/js/foundation/js/foundation.min.js"></script>
  <script src="<?php echo $url; ?>/js/app.js"></script>
  <script type="text/javascript" src="<?php echo $url; ?>/js/retina.min.js"></script>
  <script type="text/javascript" src="<?php echo $url; ?>/js/liga.js"></script>

<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//stats.meetassociated.com/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 5]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
<noscript><p><img src="//stats.meetassociated.com/piwik.php?idsite=5" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->


<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-23527436-26', 'auto');
  ga('send', 'pageview');

</script> 

