<?php 
	$url = "http://".$_SERVER['HTTP_HOST']."/Holiday";
  $img = "http://".$_SERVER['HTTP_HOST']."/Holiday/img";
  $lnk = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/Holiday";
	if ($pageType == "idx"):
		$pageTitle = "Genuine Texas Beef Holiday | Roast Recipes and Tips";
  		$pageKeywords = "";
  		$pageDescription = "Discover how easy it is to prepare a delicious Genuine Texas Beef Prime Rib or Beef Tenderloin, and make it a holiday standard. ";
  		$pageFBtitle = "";
  		$pageURL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/Holiday";
  		$pageImage = "http://".$_SERVER['HTTP_HOST']."/Holiday/img/tenderchoice_logo.png";
  		$pageFBdescription = "";
    
  elseif ($pageType == "beef-tenderloin"):
    $pageTitle = "Beef Tenderloin Recipes | Holiday Roast Recipes";
      $pageKeywords = "";
      $pageDescription = "A beef tenderloin roast is a holiday favorite that′s sure to please, so choose from one of our delicious beef tenderloin recipes!";
      $pageFBtitle = "Beef Tenderloin Recipes | Holiday Roast Recipes";
      $pageURL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/Holiday";
      $pageImage = "http://".$_SERVER['HTTP_HOST']."Holiday/img/tenderchoice_logo.png";
      $pageFBdescription = "A beef tenderloin roast is a holiday favorite that′s sure to please, so choose from one of our delicious beef tenderloin recipes!";

  elseif ($pageType == "prime-rib"):
    $pageTitle = "Prime Rib Recipes | Holiday Roast Recipes";
      $pageKeywords = "";
      $pageDescription = "A beef prime rib is the perfect cut for the most special occasions, so choose from one of our delicious prime rib recipes!";
      $pageFBtitle = "Prime Rib Recipes | Holiday Roast Recipes";
      $pageURL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/Holiday";
      $pageImage = "http://".$_SERVER['HTTP_HOST']."Holiday/img/tenderchoice_logo.png";
      $pageFBdescription = "A beef prime rib is the perfect cut for the most special occasions, so choose from one of our delicious prime rib recipes!";

  elseif ($pageType == "side-dish"):
    $pageTitle = "Holiday Beef Roast Side Dish Recipes  ";
      $pageKeywords = "";
      $pageDescription = "During the holiday season, it′s all about friends and family. Choose side dishes they′ll love and that complement your tender, juicy and flavorful beef roast. ";
      $pageFBtitle = "Holiday Beef Roast Side Dish Recipes  ";
      $pageURL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/Holiday";
      $pageImage = "http://".$_SERVER['HTTP_HOST']."Holiday/img/tenderchoice_logo.png";
      $pageFBdescription = "During the holiday season, it′s all about friends and family. Choose side dishes they′ll love and that complement your tender, juicy and flavorful beef roast. ";

  elseif ($pageType == "cook-tips"):
    $pageTitle = "Holiday Beef Roast Cooking Tips";
      $pageKeywords = "";
      $pageDescription = "Check out these cooking tips to ensure your friends and family enjoy each bite of your delicious holiday roast. ";
      $pageFBtitle = "Holiday Beef Roast Cooking Tips";
      $pageURL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/Holiday";
      $pageImage = "http://".$_SERVER['HTTP_HOST']."Holiday/img/tenderchoice_logo.png";
      $pageFBdescription = "Check out these cooking tips to ensure your friends and family enjoy each bite of your delicious holiday roast. ";


    else:
		$pageTitle = "";
  		$pageKeywords = "";
  		$pageDescription = "";
  		$pageFBtitle = "";
  		$pageURL = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/Holiday";
  		$pageImage = "http://".$_SERVER['HTTP_HOST']."/Holiday/img/tenderchoice_logo.png";
  		$pageFBdescription = "";	
	endif;



?>