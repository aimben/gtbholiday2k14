<!doctype html>
<?php 
  $pageClass = "cook-tips recipe_pg";
  $pageType = "cook-tips";
  $page2ndType = "cook-tips";
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/config.php"); 
  include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/head.php"); 
?>  
  </head>


  <body>
    <div class="off-canvas-wrap" data-offcanvas>
      <div class="inner-wrap">
        <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/nav.php'; ?>
        <a class="exit-off-canvas" href="#"></a>

        <section role="main" id="main">
          <div class="mn_img"></div>
          
          <div class="row" style="">    
             <?php include $_SERVER['DOCUMENT_ROOT'].'/Holiday/helpers/top_and_nav.php'; ?>
            
            <div class="small-12 medium-7 medium-centered text-center main_content columns">
              <h3>Holiday Roast Cooking Tips</h3>
              <p>
                A tender, juicy and flavorful Genuine Texas Beef Prime Rib or Beef Tenderloin roast is easier than you think. Check out the cooking tips below to ensure your friends and family enjoy each bite of your delicious holiday roast.
              </p>
            </div>
          </div>

          <div class="recipes">
            <div class="row">
              <div class="small-12 columns">
                <dl class="tabs vertical" data-tab data-options="deep_linking:true">
                  <dd class="active"><a href="#Beef-Tenderloin">Beef Tenderloin</a></dd>
                  <dd><a href="#Beef-Prime-Rib">Beef Prime Rib</a></dd>
                </dl>
                <div class="tabs-content" style="">
                  <div class="content active" id="Beef-Tenderloin" style="">
                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>Cooking Tips</b><br />
                        <ul>
                          <li>Cut off about 3-4&Prime; from each end to make tenderloin the same diameter from end to end</li>
                          <li>Tie the tenderloin crosswise at 2&Prime; intervals using cotton twine, season and let stand at room temperature for 1 hour</li>
                          <li>Tenderloin will continue to cook after removing from the oven; take out about 10&deg; F rarer than desired serving temperature</li>
                        </ul>

                        <br />
                        <b>Servings</b><br />
                        <ul> 
                          <li>3 lb beef tenderloin serves a party of 8</li>
                          <li>4 lb beef tenderloin serves a party of 12</li>
                        </ul>
                        <br />
                        <?php /* <small>*Serves XX depending on size of roast</small>*/ ?>

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Time</b><br />
                        <ul>
                          <li>Cook one side for about 20 minutes</li>
                          <li>Flip tenderloin and roast for 55-60 minutes</li>
                          <li>Let rest for about 10 minutes</li>
                        </ul>

                        <br />
                        <b>Temperature</b><br />
                        <ul>
                          <li>Cook beef tenderloin at 425&deg; F, until internal temperature<br />reaches 125&deg; F (medium-rare) </li>
                        </ul>
                        <br /><br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Cooking-Tips#Beef-Tenderloin" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Cooking-Tips#Beef-Tenderloin" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Cooking-Tips#Beef-Tenderloin" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>
                  </div>

                  <div class="content" id="Beef-Prime-Rib">

                    <div class="row">
                      <div class="small-12 medium-5 columns">
                        <b>Cooking Tips</b><br />
                        <ul>
                          <li>Cook the interior of the roast as slowly as possible, at as low heat as possible</li>
                          <li>Sear the exterior of the roast as quickly as possible, at as high heat as possible</li>
                        </ul>
                        <small>*Cook both phases in the oven versus in a skillet</small>

                        <br /><br /><br />
                        <b>Servings</b><br />
                        <ul>
                          <li>4 lb prime rib serves a party of 8</li>
                          <li>6 lb prime rib serves a party of 12</li>
                        </ul>
                        <br />
                       <?php /* <small>*Serves 3-12 depending on size of roast</small>*/ ?>

                      </div>

                      <div class="small-12 medium-6 columns">
                        <b>Time</b><br />
                        <ul>
                          <li>Cook roast for 3-4 hours</li>
                          <li>Take out of oven and allow to rest for 20-30 minutes<br />under aluminum foil</li>
                          
                        </ul>
                        
                        <br />
                        <b>Temperature</b><br />
                        <ul>
                          <li>Cook prime rib at 350&deg; F, until internal temperature reaches 125&deg; F (medium-rare)</li>
                          
                        </ul>
                        <br /><br />
                      <div class="social2">
                        <ul class="inline-list text-right" style="z-index:">
                          <li>Share</li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Cooking-Tips#Beef-Prime-Rib" class="st_facebook_custom fa fa-facebook-square"></a></li>
                          <li><a href="#" st_url="<?php echo $url; ?>/Cooking-Tips#Beef-Prime-Rib" class="st_twitter_custom fa fa-twitter-square"></a></li>                
                          <li><a href="#" st_url="<?php echo $url; ?>/Cooking-Tips#Beef-Prime-Rib" class="st_pinterest_custom fa fa-pinterest-square"></a></li>    
        
                        </ul>
                      </div>

                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>

        </section>
        
        

        <div class="footer">
          <div class="row">
            <div class="small-12 medium-8 columns">
              <p>
                Copyright &copy; <?php echo date("Y"); ?> Cargill, Incorporated. 151 North Main, Wichita, KS 67202. All Rights Reserved.
              </p>
            </div>
            


          </div>
        </div>  

        <?php /* include($_SERVER['DOCUMENT_ROOT']."/helpers/footer.php"); */ ?>
      </div>
    </div>

    <?php include($_SERVER['DOCUMENT_ROOT']."/Holiday/helpers/end.php"); ?>
    <script type="text/javascript">
$(document).foundationTabs({deep_linking: true});             
  </script>
   
  </body>
</html>
