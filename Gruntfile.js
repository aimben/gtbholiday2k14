module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    bless: {
      css: {
        options: {
          force: true,
          cacheBuster: false,
          compress: true
        },
        files: {
          'css/app.css': 'css/app.css'
        }
      }
    },

    copy: {
      main: {
        expand: true,
        cwd: 'bower_components',
        src: '**',
        dest: 'js'
      },
    },

    watch: {
      options: {
        livereload: true
      },     
      grunt: { files: ['Gruntfile.js'] }, 
      compass: {
        files: ['**/*.scss'],
        tasks: ['compass:dev']
      },
      js: {
        files: ['js/**/*.js'],
        tasks: ['uglify']
      },
      // Live reload files
      livereload: {
        options: { livereload: true },
        files: [
          'scss/*.scss',
          'css/*.css',
          'css/**/*.css',
          '**/*.html',
          '**/*.php',
        ]
      }      
    },

    imagemin: {
        png: {
        options: {
          optimizationLevel: 3
        },
        files: [
          {
            // Set to true to enable the following options…
            expand: true,
            // cwd is 'current working directory'
            cwd: 'img/',
            src: ['**/*.png'],
            // Could also match cwd line above. i.e. project-directory/img/
            dest: 'img/',
            ext: '.png'
          }
        ]
      },
      jpg: {
        options: {
          progressive: true
        },
        files: [
          {
            // Set to true to enable the following options…
            expand: true,
            // cwd is 'current working directory'
            cwd: 'img/',
            src: ['**/*.jpg'],
            // Could also match cwd. i.e. project-directory/img/
            dest: 'img/',
            ext: '.jpg'
          }
        ]
      }
    },

    compass: {
      dev: {
        options: {              
          sassDir: ['scss'],
          cssDir: ['css'],
          environment: 'development'    
        }
      },
      prod: {
        options: {              
          sassDir: ['scss'],
          cssDir: ['css'],
          outputStyle: 'compressed',
          environment: 'production'   
        }
      },
    }

  });

  // Default task(s).
  grunt.registerTask('default', ['compass:dev' , 'copy' , 'newer:imagemin', 'watch']);
  // prod build
  grunt.registerTask('prod', ['compass:prod', 'bless']);


};

